# Dockerfile - Maria DB

FROM ubuntu:latest

# LABELS
LABEL maitainer="Vinicius Gomes"
LABEL version="1.0"
LABEL description="ABM Produtos: base de datos ya cargada en MariaDB"

# VARIABLES PARA PASAR ARGUMENTO 
ARG DEBIAN_FRONTEND=noninteractive
 
# Instalacion y configuracion de Stack Lamp
RUN apt update && apt install -y wget mariadb-server php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip vim w3m w3m-img && apt autoremove && apt clean

# Variables de entorno para el mysql
ENV MYSQL_USER abmdbuser
ENV MYSQL_USER_PASSWORD bagaza123
ENV MYSQL_DB_NAME eduvin
 
# Creamos el directorio 'app' en el raiz
RUN mkdir -p /app

# Copiar base de datos referencia al directorio 'app'
COPY eduvin.sql /app

# Dar permisos de escritura a la base de datos
RUN chmod +r+x+w /app/eduvin.sql

# ENTRYPOINT 
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 3306

VOLUME ["/var/lib/mysql"]

CMD ["mysqld"]

