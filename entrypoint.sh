#!/bin/bash

# ABM Data base - MariaDB
# version: 1.0

# interrumpe la ejecución del script en el caso de error
set -e

# Iniciar mysql server para poder ejecutar los comandos posteriores
/etc/init.d/mysql start 

# Esperamos 5 segundos para que cargue mysql
sleep 5

# Contraseña de root # en bash ls comparaciones se hacen de dos formas
# para enteros '-eq'
# para cadena de caracteres '=='
# -f /fichero comprueba su existencia, si exite devuelve true
if [ ! -f /app/mysql.configured ]; then
	if [ $MYSQL_USER_PASSWORD == "1234" ];then
		RPASS=$((10000 + $RANDOM %30000))
		mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$RPASS';"
		touch /app/mysql.configured
	else
		mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASSWORD';"
		touch /app/mysql.configured
	fi
fi

# Crear base de datos
if [ ! -f /app/mysql.database.configured ]; then
	if [ $MYSQL_DB_NAME == "abmdb" ];then
		mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
		mysql -u root -e "source /app/eduvin.sql;"
		mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
		touch /app/mysql.database.configured
	else
		mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
		mysql -u root -e "source /app/eduvin.sql;"   
		mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
		touch /app/mysql.database.configured
	fi
fi


exec "$@"












